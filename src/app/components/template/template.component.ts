import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  public usuario: Object = {
    name: null,
    lastName: null,
    email: null,
    country: "",
    sexo: null
  };

  public countries: Object[] = [{
    code: "Col",
    name: "Colombia"
  }, {
    code: "Ven",
    name: "Venezuela"
  }, {
    code: "Per",
    name: "Peru"
  }];

  public sexs: Object[] = [{
    value: "hombre",
    name: "Hombre"
  },{
    value: "mujer",
    name: "Mujer"
  }];

  constructor() { }

  ngOnInit() {
  }

  guardar(forma: NgForm) {
    console.log("Funciona", forma.value);
  }

}
