import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormArray } from "@angular/forms";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  public forma: FormGroup;

  public usuario: Object = {
    nombreCompleto: {
      name: "Daniel",
      lastName: "Santos"
    },
    email: "Sdaniel@gmail.com",
    pasatiempos: []
  }

  constructor() {
    // new FormControl('Se pone data inicial', [validador enterado o ejecucion instantanea], [validador asincrono])

    this.forma = new FormGroup({
      'nombreCompleto': new FormGroup({
        'name': new FormControl('', [Validators.required, Validators.minLength(3)]),
        'lastName': new FormControl('', [Validators.required, this.newValidation]),
      }),
      'email': new FormControl('', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]),
      'pasatiempos': new FormArray([
        new FormControl('Correr', Validators.required)
      ]),
      'username': new FormControl('', Validators.required, this.existUsername),
      'password1': new FormControl('', Validators.required),
      'password2': new FormControl('', Validators.required),
    });

    this.forma.controls['password2'].setValidators([
      Validators.required,
      this.noIgual.bind(this.forma)
    ]);

    //Es para tener observable de un cambio general del formulario
    this.forma.valueChanges.subscribe((data: any) => {
      console.log("Esta cambiando el formulario: ", data);
    });

    this.forma.controls['username'].valueChanges.subscribe((data: any) => {
      console.log("Hay un cambio de username: ", data);
    });

    //Cuando el status esta listo
    this.forma.controls['username'].statusChanges.subscribe((data: any) => {
      console.log("Hay un cambio en su estado de username: ", data);
    });

    //Objeto que se va a llenar debe tener la misma forma
    // this.forma.setValue(this.usuario);//->ya listo papu
  }

  ngOnInit() {
  }

  saveData() {
    console.log(this.forma);

    //setear a estado pristine el form para tomar nuevas validaciones
    this.forma.reset({
      nombreCompleto: {
        name: "",
        lastName: ""
      },
      email: ""
    });
  }

  newPasatiempos() {
    // <FormArray> -> determina que es un array el objeto y le dices al ts que confie en nosotros
    (<FormArray>this.forma.controls['pasatiempos']).push(new FormControl('Dormir', Validators.required));
  }

  //crear validaciones propias
  newValidation(control: FormControl): { [s:string]:boolean } {
    if( control.value === "herrera" ) {
      return {
        newValidation: true
      }
    }

    return null;//Cuadno esta correcta la validacion
  }

  noIgual(control: FormControl): { [s:string]:boolean } {
    let form: any = this;
    
    if(control.value !== form.controls['password1'].value) {
      return {
        noIgual: true
      }
    }

    return null;
  }

  //Validaciones asincronas
  existUsername(control: FormControl): Promise<any>|Observable<any> {
    let promesa = new Promise((resolve, reject) => {
      setTimeout(() => {
        if(control.value === "strider"){
          resolve({existUsername: true});
        } else {
          resolve(null);
        }
      }, 3000);
    });
    return promesa;
  }
}
